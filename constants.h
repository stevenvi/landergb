#ifndef _CONSTANTS_H
#define _CONSTANTS_H

// Screen
#define TILE_W				8
#define TILE_H				8
#define SPRITE_START_X		8
#define SPRITE_START_Y		16
#define SCREEN_W			160
#define SCREEN_H			144
#define BG_W				152
#define BG_H				SCREEN_H
#define WIN_W				TILE_W
#define WIN_H				SCREEN_H
#define WIN_X				(SCREEN_W - TILE_W + SPRITE_START_X - 1)
#define WIN_Y				3
#define SCROLL_OFFSET_X		80
#define SCROLL_OFFSET_Y		72

// Gameplay
#define MAX_HORIZONTAL_VELOCITY	256
#define MAX_VERTICAL_VELOCITY	256
#define MAX_FUEL				((8*TILE_H)<<8)	


#endif // _CONSTANTS_H