// References specific indices into the sprites and tilesets

// References on the sprite sheet
#define SPRITESHEET_BLANK			10
#define SPRITESHEET_LANDER_L		0
#define SPRITESHEET_LANDER_R		2
#define SPRITESHEET_LANDER_LEG_L	1
#define SPRITESHEET_LANDER_LEG_R	3
#define SPRITESHEET_FLAME_BIG		4
#define SPRITESHEET_FLAME_SMALL		5
#define SPRITESHEET_FLAME_L			6
#define SPRITESHEET_FLAME_R			7
#define SPRITESHEET_VELOCITY_BAR	9
#define SPRITESHEET_EXPLOSION		8


// References on the active on-screen sprites
#define SPRITE_LANDER_L				0
#define SPRITE_LANDER_R				1
#define SPRITE_LANDER_LEG_L			2
#define SPRITE_LANDER_LEG_R			3
#define SPRITE_LANDER_FLAME_SIDE	8
#define SPRITE_LANDER_FLAME_BOTTOM	9
#define SPRITE_VELOCITY_BAR			10