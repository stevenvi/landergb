/*

 LEVEL1-MERCURY.H

 Map Include File.

 Info:
   Section       : 
   Bank          : 0
   Map size      : 32 x 32
   Tile set      : D:\Programming\Gameboy\LanderGB\res\tileset.gbr
   Plane count   : 1 plane (8 bits)
   Plane order   : Tiles are continues
   Tile offset   : 0
   Split data    : No

 This file was generated by GBMB v1.8

*/

#define level1Width 32
#define level1Height 32
#define level1Bank 0

extern unsigned char level1[];

/* End of LEVEL1-MERCURY.H */
