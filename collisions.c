#include <gb/gb.h>
#include "collisions.h"
#include "constants.h"

// Temporary global storage space for fetching tile data
extern unsigned char ucTemp;

extern UINT16 scroll_x;
extern UINT16 scroll_y;

UINT8 landing_point_test(UINT16 x, UINT16 y) {
	UINT8 tilex;
	UINT8 tiley;
	tilex = (x - SPRITE_START_X + scroll_x) >> 3;
	tiley = (y - SPRITE_START_Y + scroll_y) >> 3;
	
	// Truncate x and y to their tile-local coordinates
	x = (x + scroll_x) & 0x7;
	y = (y + scroll_y) & 0x7;
	
	get_bkg_tiles(tilex, tiley, 1, 1, &ucTemp);
	switch (ucTemp) {
		case 34:
			if (y == 6) {
				return 0x1;
			}
		default:	
			return 0x0;
	}
}

UINT8 collision_point_test(UINT16 x, UINT16 y) {
	UINT8 tilex;
	UINT8 tiley;
	UINT8 collide;
	tilex = (x - SPRITE_START_X + scroll_x) >> 3;
	tiley = (y - SPRITE_START_Y + scroll_y) >> 3;
	
	// Truncate x and y to their tile-local coordinates
	x = (x + scroll_x) & 0x7;
	y = (y + scroll_y) & 0x7;
	
	get_bkg_tiles(tilex, tiley, 1, 1, &ucTemp);
	switch (ucTemp) {
		case 33: // Full collision
			collide = 1;
			break;
			
		case 38:	 // 45 degrees lower-left collision
			if (x < y) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
		case 37:  // 45 degrees upper-right collision
			if (x > y) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;			
			
		case 39:  // 45 degrees upper-left collision
			if (x + y < 8) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;			
		case 36:  // 45 degrees lower-right collision 
			if (x + y > 8) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
			
		case 56:  // Corner upper-left collision
			if (x < 2 && y < 2) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
		case 58: // Corner upper-right collision
			if (x > 5 && y < 2) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
		case 59: // Corner lower-right collision
			if (x > 5 && y > 5) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
		case 57: // Corner lower-left collision
			if (x < 2 && y > 5) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
			
		case 42: 
			// 22.5 degrees left side downward short (verified)
			// +----------------+
			// |[]              |
			// |[]              |
			// |xx[]            |
			// |xx[]            |
			// |xxxx[]          |
			// |xxxx[]          |
			// |xxxxxx[]        |
			// |xxxxxx[]        |
			// +----------------+
			if (y > x + x) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
		case 43: 
			// 22.5 degrees left side downward wide (verified)
			// +----------------+
			// |........[]      |
			// |........[]      |
			// |..........[]    |
			// |..........[]    |
			// |............[]  |
			// |............[]  |
			// |..............[]|
			// |..............[]|
			// +----------------+
			if (y + 8 > x + x) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
			
		case 48: // 22.5 degrees top side downward short (verified)
			if (y + y < x) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
		case 50: // 22.5 degrees top side downward wide (verified)
			if (y + y < x + 8) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;

		case 44: // 22.5 degrees right side downward wide (verified)
			if (x + x > y) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
		case 45: // 22.5 degrees right side downward short (verified)
			if (x + x > y + 8) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
		
		case 53: // 22.5 degrees bottom side rightward wide (verified)
			if (y + y > x) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
		case 55: // 22.5 degrees bottom side downward short (verified)
			if (y + y > x + 8) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
		
		case 46: // 22.5 degrees right side upward short (verified)
			if (y + x + x > 16) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
		case 47: // 22.5 degrees right side upward wide (verified)
			if (y + x + x > 8) { 
				collide = 1;
			} else {
				collide = 0;
			}
			break;
		
		case 54: // 22.5 degrees top upward short (verified)
			if (y + y + x < 8) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
		case 52: // 22.5 degrees top upward wide (verified)
			if (y + y + x < 16) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
		
		case 40: // 22.5 degrees left side downward wide (verified)
			if (x + x + y < 16) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
		case 41: // 22.5 degrees left side downward short (verfied)
			if (x + x + y < 8) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
			
		case 51: // 22.5 degrees bottom rightward short
			if (y + y + x > 8) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
		case 49: // 22.5 degrees bottom rightward wide
			if (y + y + x > 16) {
				collide = 1;
			} else {
				collide = 0;
			}
			break;
		
		default:
			collide = 0;
			break;
	}
	return collide;
}
