#ifndef _SCOREBOARD_H
#define _SCOREBOARD_H

// Background positions of the scoreboard
#define SCOREBOARD_X		6
#define SCOREBOARD_Y		17
#define SCOREBOARD_WIDTH	5

// Tile index for the 0 character
#define SCOREBOARD_CHAR0	40

void init_score(void);
void increment_score(void);

#endif // _SCOREBOARD_H
