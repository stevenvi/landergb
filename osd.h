#ifndef _OSD_H
#define _OSD_H

void osd_init(void);
void update_velocity_gauge(const UINT8 previous_full_rows);
void update_fuel_gauge(const UINT8 previous_full_rows);


#endif _OSD_H