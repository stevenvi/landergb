/** Notes
 *
 * Video memory is 256x256 pixels (32x32 tiles)
 * Screen is 160x144 pixels (20x18 tiles)
 *
 * To show a sprite in the upper-left corner, it should be at (8, 16);
 */

#include <gb/gb.h>
#include "collisions.h"
#include "constants.h"
#include "indices.h"
#include "osd.h"
#include "res/level1-mercury.h"
#include "res/level2-venus.h"
#include "res/sprites.h"
#include "res/tileset.h"


void init(void);
void checkInput(void);
void updateSwitches(void);
void hide_sprite(UINT8);
UINT8 collision_check(void);
UINT8 landing_check(void);
void reset_level(void);

// The player array will hold the player's position as X ([0]) and Y ([1])
INT16 player_vel[2];		// velocity of the lander
UINT16 player_pos[2];		// fine-grained position of lander in space
UINT8 player_pos_final[2];	// final screen position to draw the lander at
UINT16 player_fuel;

UINT16 scroll_x;
UINT16 scroll_y;
UINT8 reset_pos_x;
UINT8 reset_pos_y;

UINT8 current_level;

// Counts down number of frames to wait until the level resets itself
UINT8 reset_timer;

// Temporary storage space
unsigned char ucTemp;

void main() {

	init();
	
	while(1) {
		if (reset_timer != 0) {
			reset_timer--;
			if (reset_timer == 0) {
				// Reset the level
				reset_level();
			}
		} else {
			checkInput();				// Check for user input (and act on it)
		}
		
		updateSwitches();			// Make sure the SHOW_SPRITES and SHOW_BKG switches are on each loop
		wait_vbl_done();			// Wait until VBLANK to avoid corrupting memory
	}
}

void load_level() {
	UINT8 *level;

	// Draw the background
	SWITCH_ROM_MBC1(current_level);
	set_bkg_data(0, 64, tileset);	// Load tiles into background memory (also used for window)
	switch (current_level) {
		case 1: level = level1; break;
		case 2: level = level2; break;
		default:
			level = level1;
	}
	set_bkg_tiles(0, 0, 32, 32, level);
}

void init() {
	DISPLAY_ON;						// Turn on the display
	
	current_level = 1;
	
	load_level();
	
	// Load the the 'sprites' tiles into sprite memory
	set_sprite_data(0, 16, sprites);
	
	reset_pos_x = 16;
	reset_pos_y = 16;
	
	reset_level();
}

void lander_sprite_init(void) {
	// 16x16 lander
	set_sprite_tile(SPRITE_LANDER_L, SPRITESHEET_LANDER_L);
	set_sprite_tile(SPRITE_LANDER_R, SPRITESHEET_LANDER_R);
	set_sprite_tile(SPRITE_LANDER_LEG_L, SPRITESHEET_LANDER_LEG_L);
	set_sprite_tile(SPRITE_LANDER_LEG_R, SPRITESHEET_LANDER_LEG_R);
	set_sprite_tile(SPRITE_LANDER_FLAME_SIDE, SPRITESHEET_BLANK);
	set_sprite_tile(SPRITE_LANDER_FLAME_BOTTOM, SPRITESHEET_BLANK);	
}

void lander_sprite_draw(void) {
	// 16x16 lander
	move_sprite(SPRITE_LANDER_L,     player_pos_final[0],     player_pos_final[1]);
	move_sprite(SPRITE_LANDER_R,     player_pos_final[0] + 8, player_pos_final[1]);
	move_sprite(SPRITE_LANDER_LEG_L, player_pos_final[0],     player_pos_final[1] + 8);
	move_sprite(SPRITE_LANDER_LEG_R, player_pos_final[0] + 8, player_pos_final[1] + 8);
}

void reset_level(void) {
	osd_init();
	
	lander_sprite_init();
	//set_sprite_tile(SPRITE_VELOCITY_BAR, SPRITESHEET_VELOCITY_BAR);
	//move_sprite(SPRITE_VELOCITY_BAR, 160, 50);

	player_pos[0] = ((UINT16)reset_pos_x << 8);
	player_pos[1] = ((UINT16)reset_pos_y << 8);

	player_vel[0] = 24;
	player_vel[1] = 0;
	
	player_fuel = MAX_FUEL;
	reset_timer = 0;
}

void updateSwitches() {
	SHOW_WIN;
	SHOW_SPRITES;
	SHOW_BKG;
}

void checkInput() {
	const UINT8 prev_fuel_rows = player_fuel >> 8;
	const UINT8 prev_velocity_rows = 16 - (player_vel[1] >> 4);
	const UINT8 keys = joypad();
	UINT16 tmp;
	
	// Update position
	player_pos[0] += player_vel[0];
	player_pos[1] += player_vel[1];
	
	// Position the screen to account for the player's position
	// What I'm doing here is checking if setting the screen scroll position would
	// cause wrapping, and if so, clamping it at 0.
	// Finally, scale the player's position (simulates a float)
	tmp = (player_pos[0] >> 8);
	if (tmp < SCROLL_OFFSET_X) {
		scroll_x = 0;
	} else if (tmp > (32<<3) - BG_W + SCROLL_OFFSET_X) {
		scroll_x = (32<<3) - BG_W;
	} else {
		scroll_x = tmp - SCROLL_OFFSET_X;
	}
	player_pos_final[0] = tmp - scroll_x;

	tmp = (player_pos[1] >> 8);
	if (tmp < SCROLL_OFFSET_Y) {
		scroll_y = 0;
	} else if (tmp > (32<<3) - BG_H + SCROLL_OFFSET_Y) {
		scroll_y = (32<<3) - BG_H;
	} else {
		scroll_y = tmp - SCROLL_OFFSET_Y;
	}
	player_pos_final[1] = tmp - scroll_y;
	
	// Position the bg and player
	move_bkg(scroll_x, scroll_y);
	lander_sprite_draw();

	if (collision_check() == 1) {
		set_sprite_tile(SPRITE_LANDER_L, SPRITESHEET_EXPLOSION);
		set_sprite_tile(SPRITE_LANDER_R, SPRITESHEET_EXPLOSION);
		reset_timer = 60;
	} else {
		const UINT8 lc = landing_check();
		if (lc & 0x3) {
			if (player_vel[1] < 64) {
				player_vel[0] = 0;
				player_vel[1] = 0;
				player_pos[1] &= 0xFF00;
				reset_pos_x = player_pos[0] >> 8;
				reset_pos_y = player_pos[1] >> 8;
				player_fuel += 256;
				if (player_fuel > MAX_FUEL) {
					player_fuel = MAX_FUEL;
				}
			}
		} else if (lc != 0) {
			set_sprite_tile(SPRITE_LANDER_L, SPRITESHEET_EXPLOSION);
			set_sprite_tile(SPRITE_LANDER_R, SPRITESHEET_EXPLOSION);
			reset_timer = 60;
		}
	}
	
	if (keys & J_SELECT) {
		// For now we only have two levels
		current_level++;
		if (current_level > 2) {
			current_level = 1;
		}
		load_level();
		reset_level();
	}
	
	// Check input and draw responsive graphics as needed
	if (player_fuel > 64) {
		// Button down: A
		if (keys & J_A) {
			player_vel[1] -= 5;
			set_sprite_tile(SPRITE_LANDER_FLAME_BOTTOM, SPRITESHEET_FLAME_BIG);
			move_sprite(SPRITE_LANDER_FLAME_BOTTOM, player_pos_final[0] + 4, player_pos_final[1] + 13);
			player_fuel -= 10;
		} else if (keys & J_B) {
			player_vel[1] -= 1;
			player_fuel -= 5;
			set_sprite_tile(SPRITE_LANDER_FLAME_BOTTOM, SPRITESHEET_FLAME_SMALL);
			move_sprite(SPRITE_LANDER_FLAME_BOTTOM, player_pos_final[0] + 4, player_pos_final[1] + 13);
		} else {
			hide_sprite(SPRITE_LANDER_FLAME_BOTTOM);
		}
		
		// Button down: LEFT/RIGHT
		if (keys & (J_LEFT | J_RIGHT)) {
			// Either direction gets an upward bump and loses fuel
			player_vel[1]--;
			player_fuel -= 8;
			if (keys & J_LEFT) {
				player_vel[0]--;
				set_sprite_tile(SPRITE_LANDER_FLAME_SIDE, SPRITESHEET_FLAME_R);
				move_sprite(SPRITE_LANDER_FLAME_SIDE, player_pos_final[0] + 14, player_pos_final[1] + 8);
			} else if (keys & J_RIGHT) {
				player_vel[0]++;
				set_sprite_tile(SPRITE_LANDER_FLAME_SIDE, SPRITESHEET_FLAME_L);
				move_sprite(SPRITE_LANDER_FLAME_SIDE, player_pos_final[0] - 6, player_pos_final[1] + 8);
			}
		} else {
			hide_sprite(SPRITE_LANDER_FLAME_SIDE);
		}
	} else {
		hide_sprite(SPRITE_LANDER_FLAME_SIDE);
		hide_sprite(SPRITE_LANDER_FLAME_BOTTOM);
	}	
	
	// Apply gravity
	player_vel[1] += 2;
	
	// Cap velocities
	if (player_vel[0] < -MAX_HORIZONTAL_VELOCITY) {
		player_vel[0] = -MAX_HORIZONTAL_VELOCITY;
	} else if (player_vel[0] > MAX_HORIZONTAL_VELOCITY) {
		player_vel[0] = MAX_HORIZONTAL_VELOCITY;
	}
	if (player_vel[1] > MAX_VERTICAL_VELOCITY) {
		player_vel[1] = MAX_VERTICAL_VELOCITY;
	} else if (player_vel[1] < -MAX_VERTICAL_VELOCITY) {
		player_vel[1] = -MAX_VERTICAL_VELOCITY;
	}
	
	update_velocity_gauge(prev_velocity_rows);
	update_fuel_gauge(prev_fuel_rows);	
}

void hide_sprite(UINT8 spriteIndex) {
	move_sprite(spriteIndex, 0, 0);
}

UINT8 collision_check(void) {
	// Check left leg extreme point
	return collision_point_test(player_pos_final[0] + 1,  player_pos_final[1] + 11) // Left leg
		 | collision_point_test(player_pos_final[0] + 14, player_pos_final[1] + 11) // Right leg
		 | collision_point_test(player_pos_final[0],      player_pos_final[1] + 4)  // Left side
		 | collision_point_test(player_pos_final[0] + 15, player_pos_final[1] + 4)  // Right side
		 | collision_point_test(player_pos_final[0] + 7,  player_pos_final[1]);     // Top center
}

UINT8 landing_check(void) {
	return landing_point_test(player_pos_final[0] + 1,  player_pos_final[1] + 11) 	  // Left leg
		 | (landing_point_test(player_pos_final[0] + 14, player_pos_final[1] + 11) << 1);// Right leg
}