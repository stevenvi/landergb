#ifndef _COLLISIONS_H
#define _COLLISIONS_H

UINT8 collision_point_test(UINT16 x, UINT16 y);
UINT8 landing_point_test(UINT16 x, UINT16 y);

#endif // _COLLISIONS_H
