#include <gb/gb.h>
#include "scoreboard.h"

// Temporary global storage space for fetching tile data
extern unsigned char ucTemp;

void init_score(void) {
	UINT8 i;
	ucTemp = SCOREBOARD_CHAR0;
	for (i = 0; i != SCOREBOARD_WIDTH; i++) {
		set_win_tiles(SCOREBOARD_X + i, SCOREBOARD_Y, 1, 1, &ucTemp);
	}
}

void _increment_score_recursive(UINT8 x) {
	get_win_tiles(x, SCOREBOARD_Y, 1, 1, &ucTemp);
	if (ucTemp != (SCOREBOARD_CHAR0 + 9)) {
		// Increment the digit
		ucTemp++;
		set_win_tiles(x, SCOREBOARD_Y, 1, 1, &ucTemp); 
	} else {
		// Wrap the digit
		ucTemp = SCOREBOARD_CHAR0;
		set_win_tiles(x, SCOREBOARD_Y, 1, 1, &ucTemp); 
		_increment_score_recursive(x - 1);
	}
}

void increment_score(void) {
	// Increments the score recursively
	_increment_score_recursive(SCOREBOARD_X + SCOREBOARD_WIDTH - 1);
}
