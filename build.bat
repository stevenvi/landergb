@echo Building Lander.gb...
@D:\Programming\Gameboy\Tools\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o obj/collisions.o collisions.c
@D:\Programming\Gameboy\Tools\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o obj/lander-main.o lander-main.c
@D:\Programming\Gameboy\Tools\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o obj/osd.o osd.c
@D:\Programming\Gameboy\Tools\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o obj/res_sprites.o res/sprites.c
@D:\Programming\Gameboy\Tools\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -c -o obj/res_tileset.o res/tileset.c
@D:\Programming\Gameboy\Tools\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -Wf-bo1 -c -o obj/res_level1_mercury.o res/level1-mercury.c
@D:\Programming\Gameboy\Tools\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -Wf-bo2 -c -o obj/res_level2_venus.o res/level2-venus.c
@D:\Programming\Gameboy\Tools\gbdk\bin\lcc -Wa-l -Wl-m -Wl-j -DUSE_SFR_FOR_REG -Wl-yt2 -Wl-yo16 -o ./bin/Lander.gb ./obj/*.o
@echo Done!
