#include <gb/gb.h>	
#include "osd.h"
#include "constants.h"

extern UINT16 player_vel[2];
extern UINT16 player_fuel;

void _update_gauge(const UINT8 previous_full_rows, const UINT8 full_rows, const UINT8 start_y);

// Velocity meter: 4 tiles of content
// Fuel meter: 8 tiles of content
UINT8 window_bar[18] = {19,21,30,30,22,22,31,20,21,22,22,22,22,22,22,22,22,31};

void osd_init(void) {
	// Set up the window
	set_win_tiles(0, 0, (WIN_W>>3), (WIN_H>>3), window_bar);
	move_win(WIN_X, WIN_Y);
}

void update_velocity_gauge(const UINT8 previous_full_rows) {
	// Velocity range [-256,256]
	// We show 4 tiles for velocity on-screen = 32 rows
	// 1 row then is equal to 16 units of velocity
	const UINT8 full_rows = 16 - (player_vel[1] >> 4);
	if (full_rows <= 32) {
		_update_gauge(previous_full_rows, full_rows, 5);
	}
}

void update_fuel_gauge(const UINT8 previous_full_rows) {
	const UINT8 full_rows = player_fuel >> 8;
	_update_gauge(previous_full_rows, full_rows, 16);
}

void _update_gauge(const UINT8 previous_full_rows, const UINT8 full_rows, const UINT8 start_y) {
	if (previous_full_rows != full_rows) {
		const UINT8 partial_tile_rows = full_rows & 0x7;
		UINT8 full_tiles = full_rows >> 3;
		UINT8 new_tile;
		if (previous_full_rows > full_rows) {
			// Fuel is lower
			if (partial_tile_rows == 7) {
				// Update the next tile
				new_tile = 22;
			} else {
				// Update the current tile
				new_tile = 30 - partial_tile_rows;
			}
		} else {
			// Fuel is higher
			if (partial_tile_rows == 0) {
				// We're jumping to a new tile, so finish off the previous one
				new_tile = 22;
				full_tiles--;
			} else {
				// Update a partially filled tile
				new_tile = 30 - partial_tile_rows;
			}
		}
		set_win_tiles(0, start_y - full_tiles, 1, 1, &new_tile);
	}
}